# Клонирование репозитория

Для клонирования репозитория необходимо выполнить команду: 
```
git clone https://gitlab.com/Rus_Abbasov/service.git
```

# Инструкция по работе

Все необходимые для работы ansible-playbook переменные(название образа для приложения, Access Token для входа в dockerhub, логин для входа в dockerhub) будут прописаны в файле [skillbox_app/defaults/main.yml](https://gitlab.com/Rus_Abbasov/service/-/blob/master/skillbox_app/defaults/main.yml?ref_type=heads) при выполнении pipeline из репозитория [diploma](https://gitlab.com/Rus_Abbasov/diploma)(их значения прописаны в переменных GitLab(см. [README.md репозитория infra](https://gitlab.com/Rus_Abbasov/infra/-/blob/master/README.md?ref_type=heads)))

## Запуск ansible-playbook

Запуск данного ansible-playbook происходит через pipeline, который находится в репозитории [diploma](https://gitlab.com/Rus_Abbasov/diploma)([.gitlab-ci.yml](https://gitlab.com/Rus_Abbasov/diploma/-/blob/master/.gitlab-ci.yml?ref_type=heads))
